import { createRouter } from './router';
import type { SofaConfig } from './sofa';
import { createSofa } from './sofa';

export { OpenAPI } from './open-api';

export function useSofa(config: SofaConfig) {
  return createRouter(createSofa(config));
}

export * from './ast';
export * from './common';
export * from './logger';
export * from './open-api';
export * from './parse';
export * from './router';
export * from './sofa';
export * from './subscriptions';
export * from './types';
